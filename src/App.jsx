// react
import { Outlet } from 'react-router-dom';

// css
import './App.sass';

const App = () => {
    return (
        <div className='App'>
            <Outlet></Outlet>
        </div>
    );
};

export default App;
