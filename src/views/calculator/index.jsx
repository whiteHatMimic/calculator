/* eslint-disable no-unused-vars */
// react
import { useEffect, useState } from 'react';

// css
import './index.sass';

const Calculator = () => {
    // eslint-disable-next-line no-unused-vars
    const [ number, setNumber ] = useState('0'); // 運算數
    const [ operator, setOperator ] = useState(''); // 運算符
    const [ buttonList, setButtonList ] = useState([ // 所有運算字元符號
        ['+', '7', '8', '9'],
        ['-', '4', '5', '6'],
        ['*', '1', '2', '3'],
        ['/', '0', 'C']
    ]);

    useEffect(() => {
        let pKey = 0;
        let cKey = 0;

        document.addEventListener('keydown', (e) => {
            buttonList.forEach((prevent, pIdx) => {
                prevent.forEach((children, cIdx) => {
                    if (e.key === children) {
                        calc(children);
                        pKey = pIdx;
                        cKey = cIdx;
                        handleOnKeyDownAndKeyUpStyle(pIdx, cIdx, true);
                    }

                    if (e.key === 'Escape') {
                        calc('C');
                        pKey = pIdx;
                        cKey = cIdx;
                        handleOnKeyDownAndKeyUpStyle(pIdx, cIdx, true);
                    }
                });
            });
        });

        document.addEventListener('keyup', (e) => {
            handleOnKeyDownAndKeyUpStyle(pKey, cKey, false);
        });
    });

    const handleOnKeyDownAndKeyUpStyle = (pIdx = 0, cIdx = 0, active) => {
        let btnSymbol = document.querySelectorAll('.btn.btnSymbol');
        let btnList = document.querySelectorAll('.btnList')[pIdx].querySelectorAll('.btn.btnSymbol')[cIdx];

        for(let i = 0; i < btnSymbol.length; i++) {
            btnSymbol[i].classList = 'btn btnSymbol';
        }

        if (active) {
            btnList.classList.add('active');
        }
        else {
            btnList.classList.remove('active');
        }
    };    

    /**
     * 自動計算
     * @param { string } symbol 字元符號
     */
    const calc = (symbol) => {
        if (symbol === '+' || symbol === '-' || symbol === '*' || symbol === '/') {
            setOperator(symbol);
        }
        
        if (Number.isInteger(parseInt(symbol))) {
            if (operator !== '') {
                switch (operator) {
                    case '+':
                        setNumber(parseInt(number) + parseInt(symbol));
                        break;
                    case '-':
                        setNumber(parseInt(number - symbol));
                        break;
                    case '*':
                        setNumber(parseInt(number - symbol));
                        break;
                    case '/':
                        setNumber(parseInt(number - symbol));
                        break;
                }

                setOperator('');
            }
            else {
                setNumber(symbol);
            }
        }

        if (symbol === 'C') {
            setNumber('0');
            setOperator('');
        }
    };

    return (
        <div className='calculator'>
            
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='box w-full sm:w-6/12 md:w-6/12 lg:w-6/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card'>
                            {/* 運算符 */}
                            <div className='operator w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                <h4>{ operator }</h4>
                            </div>

                            {/* 運算數 */}
                            <div className='number w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                <h3>{ number }</h3>
                            </div>

                            {/* 所有運符符、運算數 */}
                            {
                                buttonList.map((prevent, pidx) => {
                                    return (
                                        <div className='btnList w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12' key={pidx}>
                                            {
                                                prevent.map((children, cidx) => {
                                                    return (
                                                        <button className='btn btnSymbol' key={cidx} onClick={() => calc(children)}>{children}</button>
                                                    );
                                                })
                                            }
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Calculator;