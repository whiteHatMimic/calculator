// css
import './index.sass';

// components
import AppMain from './AppMain/AppMain.jsx';

const Layout = () => {
    return (
        <div className="appWapper">
            <AppMain></AppMain>
        </div>
    );
};

export default Layout;