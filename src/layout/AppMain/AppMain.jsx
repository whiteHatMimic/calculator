// react
import { Outlet } from 'react-router-dom';

// css
import './AppMain.sass';

const AppMain = () => {
    return (
        <div className='appMain'>
            <Outlet></Outlet>
        </div>
    );
};

export default AppMain;