module.exports = {
    'parserOptions': {
        'parser': 'babel-eslint',
        'sourceType': 'module',
        'ecmaVersion': 'latest',
        'ecmaFeatures': {
            'jsx': true,
            'tsx': true
        },
    },
    'env': {
        'browser': true,
        'es6': true,
        'node': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:react/recommended'
    ],
    'plugins': [
        'react'
    ],
    'overrides': [
        {
            'files': [ '*.js', '*.jsx' ],
            'rules': {
                'indent': ['error', 4, { 'SwitchCase': 1 }],
                'no-control-regex': 0
            }
        },
        {
            'files': [ '*.ts', '*.tsx' ],
            'rules': {
                'indent': ['error', 4, { 'SwitchCase': 1 }],
                'no-control-regex': 0
            }
        },
        {
            'files': [ '*.sass', '*.scss' ],
            'rules': {
                'indent': ['error', 4, { 'SwitchCase': 1 }]
            }
        }
    ],
    'rules': {
        'react/react-in-jsx-scope': 'off',
        // 'react/function-component-definition': 1,
        // 強制必須加分號
        'semi': ['error', 'always'],
        // 強制使用單引號
        'quotes': ['error', 'single'],
        // 檢查未使用變數，'vars': 'all' 檢查所有變數情況。'args': 'none' 不需檢查參數
        'no-unused-vars': [2, {
            'vars': 'all',
            'args': 'none'
        }],
        // 箭頭函式前後皆須空白
        'arrow-spacing': ['error', {
            'before': true,
            'after': true
        }],
        'block-spacing': ['error', 'never'],
        // 大括號擺放樣式
        'brace-style': ['error', 'stroustrup'],
        'no-undef': 'off'
    }
};
